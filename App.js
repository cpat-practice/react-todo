import { Button, FlatList, StyleSheet, View } from "react-native";
import { useState } from 'react';

import GoalItem from "./components/GoalItem";
import GoalInput from "./components/GoalInput";
import { StatusBar } from "expo-status-bar";

export default function App() {
  const [courseGoals, setCourseGoals] = useState([]);
  const [modalIsVisible, setModalIsVisible] = useState(false);

  function startAtGoalHandler() {
    setModalIsVisible(true);
  }

  function endAddGoalHandler() {
    setModalIsVisible(false);
  }


  function addGoalHandler(enteredGoalText) {
    setCourseGoals(currentGoals => [...currentGoals,
    { text: enteredGoalText, id: Math.random().toString() }]);
    endAddGoalHandler()
  }

  function onDeleteItem(id) {
    setCourseGoals(currentGoals => {
      return currentGoals.filter((goal) => goal.id !== id);
    });
  }

  return (
    <>
      <StatusBar style='light' />

      <View style={styles.appContainer}>

        <Button title='Add new Goal' color="#a065ec" onPress={startAtGoalHandler} />
        <GoalInput visible={modalIsVisible} onAddGoal={addGoalHandler} onCancel={endAddGoalHandler} />

        <View style={styles.goalsContainer}>
          <FlatList
            data={courseGoals}
            alwaysBounceVertical="false"
            keyExtractor={(item, index) => item.id}
            renderItem={(itemData) => {
              itemData.index;
              return (
                <GoalItem
                  text={itemData.item.text}
                  id={itemData.item.id}
                  onDeleteItem={onDeleteItem}
                />
              )
            }}
          />
        </View>
      </View >
    </>
  );
}


const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 16,
  },

  goalsContainer: {
    flex: 5,
  },

});

// create method to add two numbers
